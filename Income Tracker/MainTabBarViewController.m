//
//  MainTabBarViewController.m
//  Income Tracker
//
//  Created by Stuart Adams on 2/7/15.
//  Copyright (c) 2015 Stuart Adams. All rights reserved.
//

#import "MainTabBarViewController.h"

@interface MainTabBarViewController ()

@end

@implementation MainTabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self registerNotification];
    
    ad = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    UIBarButtonItem* addItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addIncomeExpense)];
    
    self.navigationItem.rightBarButtonItem = addItem;
    
    UILabel* balanceLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height - 133, self.view.frame.size.width/3 + 20, 20)];
    
    balanceLbl.text = @"Balance:";
    balanceLbl.backgroundColor = [UIColor whiteColor];
    balanceLbl.textAlignment = NSTextAlignmentCenter;
    
    
    balanceAmount = [[UILabel alloc]initWithFrame:CGRectMake(balanceLbl.frame.size.width + balanceLbl.frame.origin.x, balanceLbl.frame.origin.y, self.view.frame.size.width - balanceLbl.frame.size.width, 20)];
    
    balanceAmount.textAlignment = NSTextAlignmentLeft;
    balanceAmount.backgroundColor = [UIColor whiteColor];

    [self.view addSubview:balanceLbl];
    [self.view addSubview:balanceAmount];
    [self updateBalance];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)registerNotification
{
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateValues:) name:@"incomeNotification" object:nil];
}

-(void)updateValues:(NSNotification*)n
{
    [self updateBalance];
}

-(void)updateBalance
{
    balanceAmount.text = [NSString stringWithFormat:@"$ %.02f",[ad.balanceTotal floatValue]];
    
    if([ad.balanceTotal floatValue] > 0)
    {
        balanceAmount.textColor = [UIColor colorWithRed:0.09 green:0.34 blue:0.15 alpha:1.0];
    }
    else
    {
        balanceAmount.textColor = [UIColor colorWithRed:0.80 green:0 blue:0 alpha:1.0];
    }
}

-(void)addIncomeExpense
{
    AddExpenseIncomeViewController* aeivc = [AddExpenseIncomeViewController new];
    [self.navigationController pushViewController:aeivc animated:NO];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
