//
//  AddExpenseIncomeViewController.h
//  Income Tracker
//
//  Created by Stuart Adams on 2/8/15.
//  Copyright (c) 2015 Stuart Adams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface AddExpenseIncomeViewController : UIViewController<UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate>
{
    UITextField* inputAmount;
    UITextField* inputSourceName;
    
    BOOL isExpense;
    
    UILabel* amountTitleLbl;
    UILabel* inputSourceLbl;
    
    AppDelegate* ad;
    
    int lblWidth;
    int linePadding;
    int numberOfPickerCols;
    
    NSString* dateString;
    NSString* imagePath;
    
    NSArray* pickerChoices;
    
    UIPickerView* incomeOrExpense;
    UIDatePicker* dateOfInput;
    
    UIButton* takePicture;
    UIButton* addPicture;
    
    UIScrollView* scrlView;
}

@end
