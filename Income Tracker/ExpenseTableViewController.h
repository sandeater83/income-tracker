//
//  ExpenseTableViewController.h
//  Income Tracker
//
//  Created by Stuart Adams on 2/8/15.
//  Copyright (c) 2015 Stuart Adams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "ExpenseIncomeCellTableViewCell.h"
#import "AppDelegate.h"

@interface ExpenseTableViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, MFMailComposeViewControllerDelegate>
{
    UITableView* expenseTable;
    AppDelegate* ad;
}

@end
