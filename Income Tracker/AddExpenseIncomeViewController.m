//
//  AddExpenseIncomeViewController.m
//  Income Tracker
//
//  Created by Stuart Adams on 2/8/15.
//  Copyright (c) 2015 Stuart Adams. All rights reserved.
//

#import "AddExpenseIncomeViewController.h"

@interface AddExpenseIncomeViewController ()

@end

@implementation AddExpenseIncomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIBarButtonItem* saveItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveEntry:)];
    
    self.navigationItem.rightBarButtonItem = saveItem;
    
    scrlView = [UIScrollView new];
    scrlView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    scrlView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height * 1.5);
    
    pickerChoices = @[@"Income", @"Expense"];
    numberOfPickerCols = 1;
    isExpense = false;
    
    lblWidth = 100;
    linePadding = 15;
    inputSourceLbl = [[UILabel alloc]initWithFrame:CGRectMake(20, 20, lblWidth, 25)];
    
    inputSourceLbl.text = @"Source";
    
    inputSourceName = [[UITextField alloc]initWithFrame:CGRectMake(inputSourceLbl.frame.origin.x + inputSourceLbl.frame.size.width + 20, inputSourceLbl.frame.origin.y, self.view.frame.size.width - inputSourceLbl.frame.size.width - inputSourceLbl.frame.origin.x - 40, 25)];
    inputSourceName.backgroundColor = [UIColor whiteColor];
    inputSourceName.borderStyle = UITextBorderStyleRoundedRect;
    inputSourceName.returnKeyType = UIReturnKeyNext;
    [inputSourceName becomeFirstResponder];
    
    amountTitleLbl = [[UILabel alloc]initWithFrame:CGRectMake(20, inputSourceLbl.frame.origin.y + inputSourceLbl.frame.size.height + linePadding, lblWidth, 25)];
    
    amountTitleLbl.text = @"Amount";
    
    inputAmount = [[UITextField alloc]initWithFrame:CGRectMake(amountTitleLbl.frame.origin.x +  amountTitleLbl.frame.size.width + 20, amountTitleLbl.frame.origin.y, self.view.frame.size.width - amountTitleLbl.frame.size.width - amountTitleLbl.frame.origin.x - 40, 25)];
    inputAmount.keyboardType = UIKeyboardTypeDecimalPad;
    inputAmount.backgroundColor = [UIColor whiteColor];
    inputAmount.borderStyle = UITextBorderStyleRoundedRect;
    
    UIButton* doneBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [doneBtn setTitle:@"Done" forState:UIControlStateNormal];
    doneBtn.frame = CGRectMake(0, 0, self.view.frame.size.width, 30);
    [doneBtn addTarget:self action:@selector(doneEditing:) forControlEvents:UIControlEventTouchUpInside];
    doneBtn.backgroundColor = [UIColor whiteColor];
    inputAmount.inputAccessoryView = doneBtn;
    
    inputSourceName.delegate = self;
    inputAmount.delegate = self;
    
    incomeOrExpense = [[UIPickerView alloc]initWithFrame:CGRectMake(0, amountTitleLbl.frame.size.height + amountTitleLbl.frame.origin.y, self.view.frame.size.width/2, 162)];
    incomeOrExpense.delegate = self;
    incomeOrExpense.dataSource = self;
    
    dateOfInput = [[UIDatePicker alloc]initWithFrame:CGRectMake(0, incomeOrExpense.frame.size.height + incomeOrExpense.frame.origin.y, self.view.frame.size.width/2, 162)];
    dateOfInput.datePickerMode = UIDatePickerModeDate;
    
    [dateOfInput addTarget:self action:@selector(updateDateString:) forControlEvents:UIControlEventValueChanged];
    
    takePicture = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    takePicture.frame = CGRectMake(incomeOrExpense.frame.origin.x + incomeOrExpense.frame.size.width + 10, incomeOrExpense.frame.origin.y + 50, 100, 30);
    [takePicture setTitle:@"Take Picture" forState:UIControlStateNormal];
    [takePicture addTarget:self action:@selector(takePhoto:) forControlEvents:UIControlEventTouchUpInside];
    
    addPicture = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    addPicture.frame = CGRectMake(takePicture.frame.origin.x, takePicture.frame.origin.y + 50, 100, 30);
    [addPicture setTitle:@"Add Picture" forState:UIControlStateNormal];
    [addPicture addTarget:self action:@selector(selectPhoto:) forControlEvents:UIControlEventTouchUpInside];
    
    ad = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    [self.view addSubview:scrlView];
    [scrlView addSubview:takePicture];
    [scrlView addSubview:addPicture];
    [scrlView addSubview:dateOfInput];
    [scrlView addSubview:inputSourceName];
    [scrlView addSubview:amountTitleLbl];
    [scrlView addSubview:inputSourceLbl];
    [scrlView addSubview:inputAmount];
    [scrlView addSubview:incomeOrExpense];
}

-(void)saveEntry:(id)sender
{
    NSMutableDictionary* entry = [NSMutableDictionary new];
    float value = [inputAmount.text floatValue];
    
    [entry setValue:inputSourceName.text forKey:@"entryTitle"];
    [entry setValue:[NSNumber numberWithFloat:[inputAmount.text floatValue]] forKey:@"entryCost"];
    
    if (imagePath) {
        [entry setValue:imagePath forKey:@"imagePath"];
    }
    
    NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
    
    if (!dateString)
    {
        NSDateFormatter* dateFormatter = [NSDateFormatter new];
        [dateFormatter setDateFormat:@"MM-dd-yyyy"];
        dateString = [dateFormatter stringFromDate:[NSDate date]];
    }
    
    [entry setValue:dateString forKey:@"entryDate"];
    
    float currentBalance = [ad.balanceTotal floatValue];
    
    if(!currentBalance)
    {
        currentBalance = 0;
    }
    
    if(isExpense)
    {
        [ad.expenseEntries addObject:entry];
        [ud setObject:ad.expenseEntries forKey:@"expensesDataArray"];
        currentBalance -= value;
    }
    else
    {
        [ad.incomeEntries addObject:entry];
        [ud setObject:ad.incomeEntries forKey:@"incomeDataArray"];
        currentBalance += value;
    }

    ad.balanceTotal = [NSNumber numberWithFloat:currentBalance];
    [ud setObject:ad.balanceTotal forKey:@"balance"];
    [ud synchronize];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"incomeNotification" object:@"update balance"];
    
    [self.navigationController popViewControllerAnimated:NO];
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return pickerChoices.count;
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return numberOfPickerCols;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    switch (component) {
        case 0:
            return pickerChoices[row];
            break;
            
        default:
            return @"void";
            break;
    }
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (row == 0)
    {
        isExpense = false;
    }
    else
    {
        isExpense = true;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)updateDateString:(id)sender
{
    UIDatePicker* dp = (UIDatePicker*)sender;
    NSDateFormatter* dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"MM-dd-yyyy"];
    dateString = [dateFormatter stringFromDate:dp.date];
}

- (void)takePhoto:(id)sender
{
    UIImagePickerController* picker = [[UIImagePickerController alloc]init];
    picker.delegate = self;
    picker.sourceType = UIImagePickerControllerCameraCaptureModePhoto;
    [self presentViewController:picker animated:YES completion:^{
    }];
}

- (void)selectPhoto:(UIButton*) sender
{
    UIImagePickerController* picker = [UIImagePickerController new];
    picker.delegate = self;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:^{
        
    }];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage* chosenImage = info[UIImagePickerControllerOriginalImage];
    
    NSDateFormatter* df = [NSDateFormatter new];
    [df setDateFormat:@"yyyy-MM-DD HH:mm:ss"];
    
    NSString* savedPath = [self saveImage:chosenImage withTimeString:[df stringFromDate:[NSDate date]]];
    
    UIImageView* iv = [[UIImageView alloc]initWithFrame:CGRectMake(20, dateOfInput.frame.size.height + dateOfInput.frame.origin.y + linePadding, self.view.frame.size.width - 40, self.view.frame.size.width - 40)];
    
    iv.image = [self getImage:savedPath];
    
    
    
    [picker dismissViewControllerAnimated:YES completion:^{
        scrlView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height * 2);
        [scrlView addSubview:iv];
    }];
}

-(UIImage*)getImage:(NSString*)pathString
{
    return [UIImage imageWithContentsOfFile:pathString];
}

-(NSString*) saveImage:(UIImage*)myImage withTimeString:(NSString*)timeString
{
    NSData* imageData = UIImagePNGRepresentation(myImage);
    
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentDir = [paths objectAtIndex:0];
    
    imagePath = [documentDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",timeString]];

    if (![imageData writeToFile:imagePath atomically:NO])
    {
        return nil;
    }
    else
    {
        return imagePath;
    }
}

-(void)doneEditing:(id)sender
{
    [self.view endEditing:YES];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [inputAmount becomeFirstResponder];
    return true;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
