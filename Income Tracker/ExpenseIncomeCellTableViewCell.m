//
//  ExpenseIncomeCellTableViewCell.m
//  Income Tracker
//
//  Created by Stuart Adams on 2/7/15.
//  Copyright (c) 2015 Stuart Adams. All rights reserved.
//

#import "ExpenseIncomeCellTableViewCell.h"

@implementation ExpenseIncomeCellTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if(self)
    {
        moneySign = [UILabel new];
        amount = [UILabel new];
        name = [UILabel new];
        date = [UILabel new];
        
        [self addSubview:moneySign];
        [self addSubview:name];
        [self addSubview:amount];
        [self addSubview:date];
    }
    
    return self;
}

- (void)awakeFromNib {
    // Initialization code
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)setCellWidth:(int)cellWidth
          andHeight:(int)cellHeight
{
    date.frame = CGRectMake(20, 0, 100, 20);
    
    name.frame = CGRectMake(20, date.frame.origin.y + date.frame.size.height, cellWidth/2 - name.frame.origin.x, 20);
    
    moneySign.frame = CGRectMake(name.frame.size.width + name.frame.origin.x + 10 , cellHeight/2, 10, 20);
    moneySign.text = @"$";
    
    amount.frame = CGRectMake(moneySign.frame.size.width + moneySign.frame.origin.x + 10, cellHeight/2, cellWidth/2 - moneySign.frame.size.width, 20);
}

-(void)setName:(NSString*)nameIn
{
    name.text = nameIn;
}

-(void)setAmount:(NSString*)amountIn
{
    amount.text = amountIn;
}

-(void)setDate:(NSString *)dateIn
{
    date.text = dateIn;
}

//used to set text colors appropriate for income/expense
-(void)isExpense:(BOOL)b
{
    if(b)
    {
        moneySign.textColor = [UIColor colorWithRed:0.80 green:0 blue:0 alpha:1.0];
        amount.textColor = [UIColor colorWithRed:0.80 green:0 blue:0 alpha:1.0];
    }
    else
    {
        moneySign.textColor = [UIColor colorWithRed:0.09 green:0.34 blue:0.15 alpha:1.0];
        amount.textColor = [UIColor colorWithRed:0.09 green:0.34 blue:0.15 alpha:1.0];
    }
}

@end
