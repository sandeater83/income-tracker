//
//  IncomeTableViewController.m
//  Income Tracker
//
//  Created by Stuart Adams on 2/7/15.
//  Copyright (c) 2015 Stuart Adams. All rights reserved.
//

#import "IncomeTableViewController.h"

@interface IncomeTableViewController ()

@end

@implementation IncomeTableViewController

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if(self)
    {
        self.title = @"Income";
        self.tabBarItem.image = [UIImage imageNamed:@"income_icn"];
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor greenColor];
    
    [self registerNotification];
    
    ad = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    incomeTable = [[UITableView alloc]initWithFrame:CGRectMake(10, 0, self.view.frame.size.width-20, self.view.frame.size.height - 89) style:UITableViewStylePlain];
    incomeTable.delegate = self;
    incomeTable.dataSource = self;
    
    [self.view addSubview:incomeTable];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* identifier = @"cell";
    ExpenseIncomeCellTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    NSDictionary* dict = ad.incomeEntries[indexPath.row];
    
    if (!cell) {
        cell = [[ExpenseIncomeCellTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        [cell setCellWidth:self.view.frame.size.width andHeight:40];
        [cell setName:dict[@"entryTitle"]];
        [cell setAmount:[NSString stringWithFormat:@"%.02f",[dict[@"entryCost"] floatValue]]];
        [cell setDate:dict[@"entryDate"]];
        [cell isExpense:NO];
    }
    
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return ad.incomeEntries.count;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary* dict =ad.expenseEntries[indexPath.row];
    
    MFMailComposeViewController* controller = [[MFMailComposeViewController alloc]init];
    
    NSString* entryName = dict[@"entryTitle"];
    NSString* cost = [NSString stringWithFormat:@"%.02f",[dict[@"entryCost"] floatValue]];
    NSString* entryDate = dict[@"entryDate"];
    
    controller.mailComposeDelegate = self;
    
    [controller setSubject:@"What is this income?"];
    
    [controller setMessageBody:[NSString stringWithFormat:@"%@ on %@ costing %@", entryName, entryDate, cost] isHTML:NO];
    
    
    if(controller)
    {
        [self presentViewController:controller animated:YES completion:^{
            
        }];
    }
}

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

-(void)registerNotification
{
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateValues:) name:@"incomeNotification" object:nil];
}

-(void)updateValues:(NSNotification*)n
{
    NSLog(@"%@", n.object);
    [incomeTable reloadData];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
