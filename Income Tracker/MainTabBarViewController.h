//
//  MainTabBarViewController.h
//  Income Tracker
//
//  Created by Stuart Adams on 2/7/15.
//  Copyright (c) 2015 Stuart Adams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddExpenseIncomeViewController.h"
#import "AppDelegate.h"

@interface MainTabBarViewController : UITabBarController
{
    UILabel* balanceAmount;
    AppDelegate* ad;
}

@end
