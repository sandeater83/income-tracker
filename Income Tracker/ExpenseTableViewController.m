//
//  ExpenseTableViewController.m
//  Income Tracker
//
//  Created by Stuart Adams on 2/8/15.
//  Copyright (c) 2015 Stuart Adams. All rights reserved.
//

#import "ExpenseTableViewController.h"

@interface ExpenseTableViewController ()

@end

@implementation ExpenseTableViewController

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if(self)
    {
        self.title = @"Expenses";
        self.tabBarItem.image = [UIImage imageNamed:@"expense_icn"];
    }
    
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor redColor];
    [self registerNotification];
    
    ad = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    expenseTable = [[UITableView alloc]initWithFrame:CGRectMake(10, 0, self.view.frame.size.width-20, self.view.frame.size.height - 89) style:UITableViewStylePlain];
    expenseTable.delegate = self;
    expenseTable.dataSource = self;
    
    [self.view addSubview:expenseTable];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary* dict =ad.expenseEntries[indexPath.row];
    
    MFMailComposeViewController* controller = [[MFMailComposeViewController alloc]init];
    
    NSString* entryName = dict[@"entryTitle"];
    NSString* cost = [NSString stringWithFormat:@"%.02f",[dict[@"entryCost"] floatValue]];
    NSString* entryDate = dict[@"entryDate"];
    
    controller.mailComposeDelegate = self;
    
    [controller setSubject:@"What is this expense?"];
    
    [controller setMessageBody:[NSString stringWithFormat:@"%@ on %@ costing %@", entryName, entryDate, cost] isHTML:NO];
    
    
    if(controller)
    {
        [self presentViewController:controller animated:YES completion:^{
            
        }];
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* identifier = @"cell";
    ExpenseIncomeCellTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    NSDictionary* dict = ad.expenseEntries[indexPath.row];
    
    if (!cell) {
        cell = [[ExpenseIncomeCellTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        [cell setCellWidth:self.view.frame.size.width andHeight:40];
        [cell setName:dict[@"entryTitle"]];
        [cell setAmount:[NSString stringWithFormat:@"%.02f",[dict[@"entryCost"] floatValue]]];
        [cell setDate:dict[@"entryDate"]];
        [cell isExpense:YES];
    }
    
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return ad.expenseEntries.count;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)registerNotification
{
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateValues:) name:@"incomeNotification" object:nil];
}

-(void)updateValues:(NSNotification*)n
{
    [expenseTable reloadData];
    NSLog(@"updating expenses");
}

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
