//
//  ViewController.m
//  Income Tracker
//
//  Created by Stuart Adams on 2/7/15.
//  Copyright (c) 2015 Stuart Adams. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    IncomeTableViewController* ivc = [IncomeTableViewController new];
    ExpenseTableViewController* evc = [ExpenseTableViewController new];
    MainTabBarViewController* mtbvc = [MainTabBarViewController new];
    
    NSArray* vcsArray = @[ivc,
                          evc];
    
    [mtbvc setViewControllers:vcsArray];
    
    UINavigationController* nc = [[UINavigationController alloc]initWithRootViewController:mtbvc];
    
    [self.view addSubview:nc.view];
    [self addChildViewController:nc];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
