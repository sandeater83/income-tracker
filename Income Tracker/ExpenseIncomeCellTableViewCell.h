//
//  ExpenseIncomeCellTableViewCell.h
//  Income Tracker
//
//  Created by Stuart Adams on 2/7/15.
//  Copyright (c) 2015 Stuart Adams. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExpenseIncomeCellTableViewCell : UITableViewCell
{
    UILabel* name;
    UILabel* amount;
    UILabel* moneySign;
    UILabel* date;
}

-(void)setCellWidth:(int)cellWidth
        andHeight:(int)cellHeight;
-(void)setName:(NSString*)name;
-(void)setAmount:(NSString*)amount;
-(void)setDate:(NSString*)dateIn;
-(void)isExpense:(BOOL)b;

@end
